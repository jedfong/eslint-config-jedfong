module.exports = {
  env: {
    node: true,
    es6: true,
  },
  parserOptions: {
    ecmaVersion: 2021,
  },
  plugins: [
    'eslint-plugin-node',
  ],
  rules: {
    'node/callback-return': 'off',
    'node/exports-style': 'off',
    'node/file-extension-in-import': 'off',
    'node/global-require': 'off',
    'node/handle-callback-err': 'error',
    'node/no-callback-literal': 'off',
    'node/no-deprecated-api': 'off',
    'node/no-exports-assign': 'off',
    'node/no-extraneous-import': 'off',
    'node/no-extraneous-require': 'off',
    'node/no-missing-import': 'off',
    'node/no-missing-require': 'off',
    'node/no-mixed-requires': 'off',
    'node/no-new-require': 'error',
    'node/no-path-concat': 'error',
    'node/no-process-env': 'off',
    'node/no-process-exit': 'error',
    'node/no-restricted-import': 'off',
    'node/no-restricted-modules': 'off',
    'node/no-restricted-require': 'off',
    'node/no-sync': 'off',
    'node/no-unpublished-bin': 'off',
    'node/no-unpublished-import': 'off',
    'node/no-unpublished-require': 'off',
    'node/no-unsupported-features/es-builtins': 'off',
    'node/no-unsupported-features/es-syntax': 'off',
    'node/no-unsupported-features/node-builtins': 'off',
    'node/prefer-global/buffer': 'off',
    'node/prefer-global/console': 'off',
    'node/prefer-global/process': 'off',
    'node/prefer-global/text-decoder': 'off',
    'node/prefer-global/text-encoder': 'off',
    'node/prefer-global/url-search-params': 'off',
    'node/prefer-global/url': 'off',
    'node/prefer-promises/dns': 'off',
    'node/prefer-promises/fs': 'off',
    'node/process-exit-as-throw': 'off',
    'node/shebang': 'off',
  },
};
